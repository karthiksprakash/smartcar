(function gmApiService(service) {
    var baseUrl = "http://gmapi.azurewebsites.net/";
	var request = require('request');

	
	service.getData = function (getUrlSegment, id, next) {

		//Construct the post options
	    var postOptions = {
	        headers: {
	            "Content-Type": "application/json"
	        },
	        url: baseUrl + getUrlSegment,
	        json: {
	            "id": id,
	            "responseType": "JSON"
	        }
		};

		//Make the request
		try {
			request.post(postOptions, function (error, response, body) {
				next({
					"error" : error,
					"httpResponse" : response,
					"body" : body
				});
			});
	    } catch (e) {
		    next({
				"error" : e
			});
		} 
	};
	
    service.startVehicle = function(id, next) {
        service.sendCommand("actionEngineService", id, "START_VEHICLE", next);
	};

	service.stopVehicle = function (id, next) {
		service.sendCommand("actionEngineService", id, "STOP_VEHICLE", next);
	};

	service.sendCommand = function (sendUrlSegment, id, commandString, next) {
		//Construct the post options
		var postOptions = {
			headers: {
				"Content-Type": "application/json"
			},
			url: baseUrl + sendUrlSegment,
			json: {
				"id": id,
				"command": commandString,
				"responseType": "JSON"
			}
		};
		
		//Make the request
	    try {
			request.post(postOptions, function (error, response, body) {
				next({
					"error": error,
					"httpResponse": response,
					"body": body
				});
			});
	    } catch (e) {
			next({
				"error" : e
			});
	    } 
	};

})(module.exports);