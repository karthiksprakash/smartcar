﻿
describe("SmartCarController unit tests", function name() {
	var smartCarServer;
	
	before(function () {
		mockery.enable({
			warnOnReplace: false,
			warnOnUnregistered: false,
			useCleanCache: true
		});
		
		// replace the module `GMService` with a stub object
		mockery.registerMock("../services/GMService.js", "./fakeGMService.js");

		smartCarServer = require("../../index.js");
	});
	
	after(function () {
		mockery.disable();
	});
	
	//Unit tests here
	
	
});