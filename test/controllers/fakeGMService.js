(function gmApiService(service) {
    
	service.getData = function (getUrlSegment, id, next) {
		next({
			"error": null,
			"httpResponse": null,
			"body": {
				"service": "getVehicleInfo",
				"status": "200",
				"data": {
					"vin": {
						"type": "String",
						"value": "123123412412"
					},
					"color": {
						"type": "String",
						"value": "Metallic Silver"
					},
					"fourDoorSedan": {
						"type": "Boolean",
						"value": "True"
					},
					"twoDoorCoupe": {
						"type": "Boolean",
						"value": "False"
					},
					"driveTrain": {
						"type": "String",
						"value": "v8"
					}
				}
			}
		});
	};
	
    service.startVehicle = function(id, next) {
        service.sendCommand("actionEngineService", id, "START_VEHICLE", next);
	};

	service.stopVehicle = function (id, next) {
		service.sendCommand("actionEngineService", id, "STOP_VEHICLE", next);
	};

	service.sendCommand = function (sendUrlSegment, id, commandString, next) {
		next({
			"error": null,
			"httpResponse": response,
			"body": {
				"service": "actionEngine",
				"status": "200",
				"actionResult": {
					"status": "EXECUTED"
				}
			}
		});
	};

})(module.exports);