
var smartCarServer = require("../../index.js");

describe("SmartCarServer functional tests", function name() {
	this.timeout(10000);
	
	it("should get vehicle info for id \"1234\"", function (done) {
		chai.request(smartCarServer).get("/vehicles/1234").end(function (err, res) {
		    if (err)
		        console.log(err);
		    else {
				//console.log(res);
				assert(res.body);
		        assert(res.body.vin === "123123412412");
			}
		    done();
		});
	});

	it("expect error when getting vehicle info for id \"1236\"", function (done) {
		chai.request(smartCarServer).get("/vehicles/1236").end(function (err, res) {
		    if (err) {
		        //console.log(err);
		        assert(res.status === 404);
		    } else {
		        console.log(res);
		        assert(false, "A response was unexpected.");
		    }
		    done();
		});
	});

	after(function () {
	    smartCarServer.close();
	});
	
});
