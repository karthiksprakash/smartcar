﻿global.chai = require("chai");
global.assert = chai.assert;
global.chaiHttp = require("chai-http");
chai.use(chaiHttp);
chai.config.includeStack = true;
global.sinon = require("sinon");
global.mockery = require("mockery");

process.env.NODE_ENV = 'test';