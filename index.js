﻿//var gmService = require('./GMService.js');
//    gmService.getData("getVehicleInfoService", "1234", function(gmApiResponse) {
//    console.log(gmApiResponse.body);
//});

var http = require("http");
var smartCarController = require('./controllers/SmartCarController.js');
var express = require('express');
var bodyParser = require('body-parser');

//Initialize express services
var app = express();
app.use(bodyParser.json());

//Map the routes
smartCarController.init(app);


// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
	return null;
});

//Start listening
var server = http.createServer(app);
server.listen(8080, function () {
    console.log("SmartCar server listening on http://localhost:8080");
});

module.exports = server;