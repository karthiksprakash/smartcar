(function smartCarController(controller){
	var gmService = require('../services/GMService.js');
	
    controller.init = function(app) {

        //Setup routes
        app.get("/vehicles/:id", vehicleInfoHandler);
		app.get("/vehicles/:id/doors", doorsInfoHandler);
		app.get("/vehicles/:id/fuel", fuelInfoHandler);
		app.get("/vehicles/:id/battery", batteryInfoHandler);
        app.post("/vehicles/:id/engine", engineCommandHandler);
		
		//Vehicle info route handlers
        function vehicleInfoHandler(request, response) {
            var id = request.params.id;
            gmService.getData("getVehicleInfoService", id, function(gmApiResponse) {
                //console.log(gmApiResponse.body);

                //Basic error handling
				if (gmApiResponse.error) {
					onError(gmApiResponse.error, response);
                } else {
                    //On successfully receiving response from GM api.
                    response.set("Content-Type", "application/json");
                    if (gmApiResponse.body.status == "200") {
                        var vehicleData = gmApiResponse.body.data;
                        response.send({
                            "vin": vehicleData.vin.value,
                            "color": vehicleData.color.value,
                            "doorCount": (vehicleData.fourDoorSedan.value) ? 4 : 2,
                            "driveTrain": vehicleData.driveTrain.value
                        });
                    } else {
                        response.status(gmApiResponse.body.status);
                        response.send(gmApiResponse.body); //Not sure what the requirement is here. But a slight modification of GM's response is good.           
                    }
                }
            });
		};

		//Vehicle doors info handler
		function doorsInfoHandler(request, response) {
			var id = request.params.id;
			gmService.getData("getSecurityStatusService", id, function (gmApiResponse) {
				
				//Basic error handling
				if (gmApiResponse.error) {
					onError(gmApiResponse.error, response);
				} else {
					//On successfully receiving response from GM api.
					response.set("Content-Type", "application/json");
				    if (gmApiResponse.body.status == "200") {
						var vehicleData = gmApiResponse.body.data;
						var smartCarResponse = [];
						for (i = 0; i < vehicleData.doors.values.length; i++) {
							smartCarResponse.push({
								"location": vehicleData.doors.values[i].location.value,
								"locked": vehicleData.doors.values[i].locked.value === "True" //This may need some generalization later
							});
						}
				        response.send(smartCarResponse);
				    } else {
						response.status(gmApiResponse.body.status);
						response.send(gmApiResponse.body); //Not sure what the requirement is here. But a slight modification of GM's response is good.           
					}
				}
			});
		};

		//Vehicle fuel info handler
		function fuelInfoHandler(request, response) {
			var id = request.params.id;
			gmService.getData("getEnergyService", id, function (gmApiResponse) {
				
				//Basic error handling
				if (gmApiResponse.error) {
					onError(gmApiResponse.error, response);
				} else {
					//On successfully receiving response from GM api.
					response.set("Content-Type", "application/json");
					if (gmApiResponse.body.status == "200") {
						var vehicleData = gmApiResponse.body.data;
						response.send({
							"percent" : vehicleData.tankLevel.value
						});
					} else {
						response.status(gmApiResponse.body.status);
						response.send(gmApiResponse.body); //Not sure what the requirement is here. But a slight modification of GM's response is good.           
					}
				}
			});
		};

		//Vehicle fuel info handler
		function batteryInfoHandler(request, response) {
			var id = request.params.id;
			gmService.getData("getEnergyService", id, function (gmApiResponse) {
				
				//Basic error handling
				if (gmApiResponse.error) {
					onError(gmApiResponse.error, response);
				} else {
					//On successfully receiving response from GM api.
					response.set("Content-Type", "application/json");
					if (gmApiResponse.body.status == "200") {
						var vehicleData = gmApiResponse.body.data;
						response.send({
							"percent" : vehicleData.batteryLevel.value
						});
					} else {
						response.status(gmApiResponse.body.status);
						response.send(gmApiResponse.body); //Not sure what the requirement is here. But a slight modification of GM's response is good.           
					}
				}
			});
		};

		//Engine command handler
		function engineCommandHandler(request, response) {
			var id = request.params.id;
			var action = request.body.action;

		    if (action === "START")
				gmService.startVehicle(id, actionResponseHandler);
			else if(action === "STOP")
				gmService.stopVehicle(id, actionResponseHandler);

			function actionResponseHandler (gmApiResponse) {
				
				//Basic error handling
				if (gmApiResponse.error) {
				    onError(gmApiResponse.error, response);
				} else {
					//On successfully receiving response from GM api.
					response.set("Content-Type", "application/json");
					if (gmApiResponse.body.status == "200") {
						var actionResult = gmApiResponse.body.actionResult;
						response.send({
							"status": (actionResult.status === "EXECUTED") ? "success" : "error"
						});
					} else {
						response.status(gmApiResponse.body.status);
						response.send(gmApiResponse.body); //Not sure what the requirement is here. But a slight modification of GM's response is good.           
					}
				}
			}
		};

		function onError(error, response) {
			//TODO: Revisit this
		    //console.log(error);
			response.send(500, { "reason": "Unable to retrieve data from GM. " + ((typeof error === 'string')? error : JSON.stringify(error)) });
		}
    };

})(module.exports);







